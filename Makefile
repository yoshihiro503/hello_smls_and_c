CC = gcc
CFLAGS =
SMLSHARP = smlsharp
SMLSHARP_LIB=/usr/local/lib/smlsharp
TARGETS = hw
SMI = hw.smi
DEPEND = depend
OBJS = caller.o setter.o

.SUFFIXES:
.SUFFIXES: .c .sml .smi .o .smi.o

.PHONY : all clean

all : $(DEPEND) $(TARGETS)

clean :
	rm -f $(TARGETS) $(DEPEND) *.o

$(TARGETS): $(OBJS)
	$(CC) $^ $(SMLSHARP_LIB)/libsmlsharp.a -ldl -lgmp -lm -o $@

depend: $(SMI)
	$(SMLSHARP) -Ml $(SMI) > depend

.c.o:
	$(CC) $(CFLAGS) -c $<

.sml.o:
	$(SMLSHARP) -c $<

.smi.smi.o:
	$(SMLSHARP) -c $<

-include $(DEPEND)
