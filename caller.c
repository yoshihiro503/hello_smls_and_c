#include <stdio.h>

void SMLmain(void);
void (*sml_print)(char *);

int main(int argc, char **argv)
{
	sml_init(argc, argv);

	/* SML# sets function to C */
	SMLmain();

	/* C calls SML# function */
	char *str = "Hello World\n";
	sml_print(str);

	sml_finish();

	return 0;
}
