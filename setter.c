/* set function headder */
void set_print(void (*f)(char *));

/* global variable for SML# function */
void (*sml_print)(char *);

/* set function */
void set_print(void (*f)(char *))
{
	sml_print = f;
}

