val setPrint = _import "set_print" : (char ptr -> unit) -> unit
val () = setPrint (print o SMLSharpRuntime.str_new)
